# Exports

Mobilizon can natively export data to basic formats such as ICS and CSV. Producing PDF and ODS files requires 3rd-party dependencies written in Python. Mobilizon calls Python code to generate the files.
This is completely optional and Mobilizon can run without support for these formats.

!!! note "Docker"
    The dependencies are already installed and pre-configured inside the Docker image. You may still need to [add configuration](#configure_exports) if and only if you have edited the advanced configuration.

## Install dependencies

### Python version

Your Python version needs to be at least 3.6.

### PDF export: `weasyprint`

Install some libraries needed for PDF rendering.
=== "Debian"
```sh
sudo apt install python3-pip python3-cffi python3-brotli libpango-1.0-0 libpangoft2-1.0-0 libjpeg-dev zlib1g-dev
```


Install the Python module itself.
```sh
sudo -i -u mobilizon pip3 install weasyprint
```


### ODS support: `pyexcel-ods3`

Install some libraries needed for generating ODS data.
=== "Debian"
```sh
sudo apt install python3-pip libxml2-dev libxslt-dev
```

Install the Python module itself.
```sh
sudo -i -u mobilizon pip3 install pyexcel-ods3
```

## Configure exports

Add to your configuration file the following:
```elixir
config :mobilizon, :exports,
  formats: [
    Mobilizon.Service.Export.Participants.CSV,
    Mobilizon.Service.Export.Participants.PDF,
    Mobilizon.Service.Export.Participants.ODS
  ]
```

Of course, you may choose to dismiss some if you want or need to. The only format exported by default is `CSV`.

If you need to override the path where exports are temporarily saved, you can by adding the `path` key. The default production value is `"/var/lib/mobilizon/uploads/exports"`:
```elixir
config :mobilizon, :exports,
  path: "/somewhere/else",
  # [...]
```

## Create folders
Adapt the path if you changed it at the previous step.
```sh
sudo -u mobilizon mkdir -p /var/lib/mobilizon/uploads/exports/csv
sudo -u mobilizon mkdir -p /var/lib/mobilizon/uploads/exports/pdf
sudo -u mobilizon mkdir -p /var/lib/mobilizon/uploads/exports/ods
```

## Restart Mobilizon

Finally, restart Mobilizon to apply the new configuration and test exports to check everything works correctly.