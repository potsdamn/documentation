# Pictures

Mobilizon can use some 3rd-party pictures as illustrations. Right now it's only used as a illustration for the city/region the user has been located to.
Only Unsplash is supported at the moment.

## Unsplash

You need to get [an API Key from Unsplash](https://unsplash.com/developers). Unreviewed apps are only allowed 50 requests per hour.

```elixir
config :mobilizon, Mobilizon.Service.Pictures.Unsplash,
  app_name: "Mobilizon",
  access_key: "your-unsplash-API-key",
  csp_policy: [
    img_src: ["images.unsplash.com"]
```