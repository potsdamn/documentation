# Create a profile

Mobilizon gives you the opportunity to deal with multiple identities by using different profiles. To create another profile, follow these steps:

  1. go to your account settings (by clicking your avatar and **My account**)
  * click **New profile** in the left sidebar
  * click the **Click to upload** button to add an avatar for your new profile
  * enter a **Display name** (required): what other people will see
  * enter a **Username** (required): your unique identifier for your account on this and all the other instances. It's as unique as an email address, which makes it easy for other people to interact with it.
  * enter a **Bio**: tell people more about you
  * click **Save** button.

![rose boreal profile cration img](../../images/en/rose-boreal-settings-filled-EN.png)

From now on, you can participate in an event, or create one, with one or another identity.

!!! note
    You can switch between your different identities from the top bar by clicking your avatar. By default, the one displayed on the top sidebar is the one selected for your actions.

!!! note
    You have to refresh the page to see your new profile picture.
